import requests


def _url(path):
    return 'http://bungider.com' + path

def get_bungalows():
    return requests.get(_url('/places'))