import os

from httmock import response, urlmatch


NETLOC = r'(.*\.)?bungider\.com$'
HEADERS = {'content-type': 'application/json'}
GET = 'get'


class Resource:
    def __init__(self, path):
        mock_dir = os.path.dirname(os.path.abspath(__file__))
        self.path = os.path.join(mock_dir, path)

    def get(self):
        with open(self.path, 'rb') as f:
            content = f.read()
        return content


@urlmatch(netloc=NETLOC, method=GET)
def resource_get(url, request):
    file_path = url.netloc + url.path
    try:
        content = Resource(file_path).get()
    except EnvironmentError:
        return response(404, {}, HEADERS, None, 5, request)
    return response(200, content, HEADERS, None, 5, request)