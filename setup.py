from setuptools import setup, find_packages

setup(name='bungider_api',
     version='0.1',
     description='API для работы с bungider.com',
     author='R. Afanaskin',
     author_email='r.afanaskin@yandex.ru',
     license='MIT',
     packages=find_packages(),
     zip_safe=False
)